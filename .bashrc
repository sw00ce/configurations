#      __               __
#     / /_  ____ ______/ /_  __________
#    / __ \/ __ `/ ___/ __ \/ ___/ ___/
# _ / /_/ / /_/ (__  ) / / / /  / /__
#(_)_.___/\__,_/____/_/ /_/_/   \___/
#
# Created by: Papermachine (Karl)
#

# +-- Initial Configurations --+
# Interactive?
[ -z "$PS1" ] && return
stty -ixon # Disable ctrl-s and ctrl-q.
shopt -s checkwinsize # update the values of LINES and COLUMNS.


# +-- Aliases --+
# Aliases sourced in .config/aliasrc
if [ -f $HOME/.config/aliasrc ]; then
    . $HOME/.config/aliasrc
else
    echo "~/.aliasrc not found!"
fi

# +-- Bash completion and Helpers --+
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

# +-- HISTORY --+
shopt -s histappend
# Dont append duplicate history or spaces
# ignoreboth == ignoredups and ignorespace
HISTCONTROL=ignoreboth

# +-- PS1 Styling --+
# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac
[[ -f ~/.Xresources ]] && xrdb -merge ~/.Xresources
# xrdb colors are sourced from ~/.Xresources, these need not be changed.
# xrdb Colors
red='\[\e[0;31m\]'
RED='\[\e[1;31m\]'
blue='\[\e[0;34m\]'
BLUE='\[\e[1;34m\]'
cyan='\[\e[0;36m\]'
CYAN='\[\e[1;36m\]'
green='\[\e[0;32m\]'
GREEN='\[\e[1;32m\]'
yellow='\[\e[0;33m\]'
YELLOW='\[\e[1;33m\]'
PURPLE='\[\e[1;35m\]'
purple='\[\e[0;35m\]'
nc='\[\e[0m\]'
# PS1
if [ "$UID" = 0 ]; then
    PS1="$red\u$nc@$red\H$nc:$CYAN\w$nc\\n$red#$nc "
else
    PS1="$PURPLE\u$nc@$CYAN\H$nc:$GREEN\w$nc\\n$GREEN\$$nc "
fi

# keep this line at the bottom of ~/.bashrc
[ -x /bin/fish ] && SHELL=/bin/fish exec /bin/fish
