#!/bin/sh
#                  __ _ _
#    _ __ _ _ ___ / _(_) |___
#  _| '_ \ '_/ _ \  _| | / -_)
# (_) .__/_| \___/_| |_|_\___|
#   |_|
#
# Created by: Papermachine
#
##### VARIABLES #####################
export VIMRC="~/.vimrc"
# haskell cabal
export CABALDIR="$HOME/.cabal"

##### PATH ##########################
# dotbin
export PATH="$PATH:$HOME/.bin"
# web-bin
export PATH="$PATH:$HOME/.wbin"
# local-bin
export PATH="$PATH:$HOME/.local/bin"
# scripts
export PATH="$PATH:$HOME/.scripts"
# add sbin to PATH if not already
[ -z "${PATH##*/sbin*}" ] || PATH=$PATH:/sbin:/usr/sbin
# haskell cabal
export PATH="$PATH:$CABALDIR/bin"

##### PREFERENCES ##################
export EDITOR="vim"
export PAGER="less"
