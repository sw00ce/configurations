#!/bin/sh

# Displays a searchable list of all man pages available on the system.

# Big thanks to Luke Smith for showing me how to do this.

# apropos . gets all man pages
# dmenu -l 45 displays a selectable menu of all the man pages
# awk '{print $1}' prints the first word of the output
# xargs -r will only run the following command if there is an input
# | man -Tpdf outputs the man page into a pdf stream
# zathura -  displays a PDF stream

apropos . | rofi -dmenu -l 45 | awk '{print $1}' | xargs -r man -Tpdf | zathura -
