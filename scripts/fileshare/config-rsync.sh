#!/bin/sh

DATE="$(date +"%F")"

# Save destination config target
SAVCONF="/tmp/$USER-config/$DATE/"
mkdir -p "$SAVCONF"

rsync $2 $SAVECONF

# Copy new in
rsync --update $1 $2
