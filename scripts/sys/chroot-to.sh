#!/bin/sh

chroot_dir=$1

enter(){
	cp /etc/resolv.conf ${chroot_dir}/etc/resolv.conf
	mount -v -t proc /proc "${chroot_dir}/proc"
	mount -vR /sys "${chroot_dir}/sys"
	mount -vR /dev "${chroot_dir}/dev"
	mount -vR /run "${chroot_dir}/run"

	chroot "${chroot_dir}"
}

leave(){
	umount -vl "${chroot_dir}/proc"
	umount -vl "${chroot_dir}/sys"
	umount -vl "${chroot_dir}/dev"
	umount -vl "${chroot_dir}/run"
}

trap leave INT TERM

enter
