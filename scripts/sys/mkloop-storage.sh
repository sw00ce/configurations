#!/bin/sh

INPUT="/dev/zero"
OUTPUT="/tmp/loopback-image.img"

if [ ! -z "$1" ]; then
	INPUT=$1

	dd if=$INPUT of=$OUTPUT status=progress
	# Create new loopback file
	losetup -fP $OUTPUT
	# List loop devices
	losetup -a

fi
