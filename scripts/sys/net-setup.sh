#!/bin/sh

SYSIP=""
NETMASK=""
BROADCAST=""
GATEWAY=""
NAMESERVERS=""

menu() {
	# Wait for input

	read -n 1 -p "(net-setup) " menuinput
}

# weenie no-sudo mode
echo "Welcome to the net-config tool"
read -n 16 -p "System IP address: " SYSIP
read -n 16 -p "Netmask: " NETMASK
read -n 16 -p "Broadcast: " BROADCAST
read -n 16 -p "Gateway: " GATEWAY

echo "iconfig eth0 ${SYSIP} broadcast ${BROADCAST} netmask ${NETMASK} up"
echo "route add default gw ${GATEWAY}"

# Put your nameserver settings in the following:
echo "nano -w /etc/resolv.conf"
