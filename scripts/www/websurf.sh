#!/bin/sh

engines="https://start.duckduckgo.com/"

search=$(echo $engines | rofi -dmenu -p "Search for")
# leave now if nothing is being searched
[[ -z $search ]] && exit

surf $search
